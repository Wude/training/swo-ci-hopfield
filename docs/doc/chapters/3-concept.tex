\chapter{Konzeption}
\label{sec:Concept}

% ------------------------------------------------------------------------------
\section{Funktionsweise von Hopfield-Netzen}
\label{sec:hopfield}

Ein Hopfield-Netz ist ein rekurrentes künstliches Neuronales Netzwerk. Es besteht aus genau einer Schicht von Neuronen, wobei alle Neuronen miteinander verbunden sind - es ist total vernetzt. Das heißt, dass der Zustand eines Neurons unter zusätzlicher Eingabe der Zustände der anderen Neuronen berechnet wird. Abbildung~\ref{fig:hopfield_3} zeigt ein kleines Beispiel mit 3 Neuronen, darin werden die in Verarbeitung befindlichen Bits als $x_1$, $x_2$ und $x_3$ bezeichnet. Diese Bits werden zu Beginn mit den Eingabewerten vorbelegt und nach Abschluss als Ausgabewerte genutzt.
% In der Abbildung sind die Eingabe-Bits des wiederherzustellenden Musters als $i_1$, $i_2$ und $i_3$ bezeichnet. Die Zwischenergebnisse der einzelnen Neuronen werden als $n_1$, $n_2$ und $n_3$. Die zum Abschluß ausgebenen Bits sind mit $o_1$, $o_2$ und $o_3$ bezeichnet.

\begin{figure}[!ht]
	\centering
	\caption{Hopfield-Netz mit 3 Neuronen}
	\label{fig:hopfield_3}

	\includegraphics[width=0.9\textwidth]{./diagrams/hopfield.pdf}

	\begin{tabular}{r@{: }l}
		$x_i$		& Ein einzelnes Zustandsbit
	\end{tabular}
\end{figure}

\subsection{Geschichte}

Die Hopfield-Netze sind benannt nach John Hopfield, der durch den Artikel \enquote{Neural networks and physical systems with emergent collective computational abilities}, \cite{hopfield_1982_neural_networks} im Jahr 1982 diese Art von Neuralen Netzwerk bekannt machte. Seine Arbeit basiert auf einem statistischen Modell des Ferromagnetismus, das nach Ernst Ising benannt wurde - das Ising-Modell.

\subsection{Energie}

Für jeden Zustand des Hopfield-Netzes lässt sich ein skalarer Wert berechnen, der die Energie in diesem Zustand beschreibt. Die Berechnung dieser Energie, zeigt die Formal~\ref{eqn:energy} (siehe Formel (7) in \cite{hopfield_1982_neural_networks}). Die Energie von Folgezuständen liegt immer niedriger, als bei den vorigen Zuständen. Letztlich erreicht die Energie bei der Berechnung von Folgezuständen ihr Minimum und dabei auch einen letzten Zustand. Es kann auch ein Sonderfall entstehen, bei dem sich zwei letzte Zustände wechselseitig ablösen, ihr Energiewert ist dann aber gleich.

\begin{equation}
	\label{eqn:energy}
	E = -\frac{1}{2} \sum_{i=1}^{N} \sum_{j=1}^{N} w_{ij} x_i x_j
\end{equation}

\begin{tabular}{r@{: }l}
	$E$			& Die Energie für den gegebenen Zustand \\
	$N$			& Anzahl der Neuronen oder auch Anzahl der Bits eines Musters \\
	$w_{ij}$	& Ein einzelnes Gewicht
\end{tabular}

\subsection{Musterwiederherstellung}

Die Arbeitsweise bei der Musterrekonstruktion kann synchron oder asynchron sein.

\begin{description}
	\item[Synchron:] Die Ausgaben der Neuronen werden alle gemeinsam berechnet.
	\item[Asynchron:] Die Ausgaben der Neuronen werden einzeln und in Reihe berechnet, wobei die Reihenfolge zufällig bestimmt oder aber auch vorab festgelegt sein kann.
\end{description}

Die Berechnung der Ausgabe eines einzelnen Neurons zeigt die Formel~\ref{eqn:restore}. Wenn sich nach einem Berechnungsdurchlauf die Ergebnisse nicht änderen, oder aber die Ergebnisse zwischen zwei Möglichkeiten wechseln, ist der Wiederherstellungsversuch abgeschlossen.

\begin{equation}
	\label{eqn:restore}
	x_{i} \gets
	\begin{cases}
		1	& \text{wenn } \sum_{j}^{N} w_{ij} x_j > 0 \\
		-1	& \text{sonst}
	\end{cases}
\end{equation}

\subsection{Lernen von Mustern}

Für das Lernen der verschiedenen Muster kommen zwei Lernregeln zum Einsatz.

% \subsection{Hebbsches Lernen}
\subsubsection{Hebbsches Lernen}

Bei der von Donald Hebb eingeführten Hebbschen Lernregel werden die Bits eines zu lernenden Musters miteinander multipliziert und auf das jeweilige Gewicht aufaddiert. Die Formel~\ref{eqn:hebbian_learning} zeigt die Berechnung einzelner Gewichte.

\begin{equation}
	\label{eqn:hebbian_learning}
	w_{ij} = \frac{1}{P} \displaystyle \sum_{\mu=1}^{P} \epsilon_i^{\mu} \epsilon_j^{\mu} \text{ mit } i \not= j
\end{equation}

\begin{tabular}{r@{: }l}
	$\mu$				& Das zu lernende Muster \\
	$P$					& Anzahl der Muster \\
	$\epsilon_i^{\mu}$	& Das $i$-te Bit des derzeitigen Musters
\end{tabular}

% \subsection{Storkeys Lernen}
\subsubsection{Storkeys Lernen}

Die von Amos Storkey eingeführte Lernregel ermöglicht eine größere Kapazität des Hopfield-Netzes. Die Grundlagen dieser Lernregel sind zu finden im Artikel \enquote{The Basins of Attraction of a new Hopfield Learning Rule}, \cite{storkey_valabregue_1999_basins}. Bei dieser Lernregel werden zusätzlich lokale Felder berücksichtigt, die über die bereits gesammelten Gewichte gebildet werden. Die einzelen Werte werden dabei durch die Anzahl der Neuronen geteilt. Die Berechnung eines einzelnen Gewichts wird in Formel~\ref{eqn:storkey_learning} gezeigt und die Berechnung eines lokalen Feldes ist in Formel~\ref{eqn:storkey_local_field} zu finden.

\begin{equation}
	\label{eqn:storkey_learning}
	w_{ij}^0 = 0 \text{ } \forall i,j
	\text{ und } w_{ij}^{\mu} = w_{ij}^{\mu-1}
	+ \frac{1}{N} \epsilon_i^{\mu} \epsilon_j^{\mu}
	- \frac{1}{N} \epsilon_i^{\mu} h_{ji}^{\mu}
	- \frac{1}{N} h_{ij}^{\mu} \epsilon_j^{\mu}
\end{equation}

\begin{tabular}{r@{: }l}
	$w_{ij}^{\mu - 1}$	& Einzelnes Gewicht vor Berücksichtigung eines neuen Musters
\end{tabular}

\begin{equation}
	\label{eqn:storkey_local_field}
	h_{ij}^\mu = \sum_{n=1,n\not=i,j}^{N} w_{in}^{\mu-1} \epsilon_n^{\mu}
\end{equation}

\begin{tabular}{r@{: }l}
	$h_{ij}^{\mu}$		& Das lokale Feld zum $i$-ten Neuron
\end{tabular}

% ------------------------------------------------------------------------------
\section{Testbedingungen}
\label{sec:test_conditions}

% Für alle Tests soll Storkeys Lernregel zum Einsatz kommen.

\subsection{Musterauswahl}

Insgesamt werden 10 Muster gesucht, die in Form eines Bildes $13 \times 13$ Punkte und damit 169 Bits Größe haben.

Die Vektoren, die die verschiedenen Muster beschreiben sollen dabei zueinander möglichst orthogonal sein. Das heißt, das bei der Multiplikation mit anderen Mustervektoren ein möglichst geringer Absolutwert herauskommen sollte. Im Idealfall wäre also für alle Kombinationen zweier Muster die Bedingung $\abs{\vec{x_i} \cdot \vec{x_j}} < c N$ erfüllt, wobei $N$ Anzahl der Bits/Neuronen ist und $c$ ein konstanter Schwellwertfaktor (z.B.: $0{,}02$). Der Hintergrund dieser Bedingung ist, dass die Muster möglichst gut voneinander unterscheidbar sein sollten, und Orthogonalität ist dafür ein Maßstab.

Zunächst werden Muster von Hand durch Probieren gesammelt. Die Herausforderung hierbei ist, die Einhaltung der Anforderungen an die Orthogonalität dieser so gesammelten Muster.

Desweiteren soll zusätzlich ein Satz von 10 Mustern zum Einsatz kommen, der durch eine automatisierte Suche ermittelt wurde und dessen Muster alle die oben genannte Bedingung einhalten. Diese Muster sehen nach menschlichen Maßstäben selbst wie Rauschen aus, aber mit ihnen sollten dennoch der Erwartung nach die besten Ergebnisse erzielt werden können.

\subsection{Verrauschung}

Zum Test werden die Muster verrauscht. Dabei wird ein fest vorgegebener Anteil der Bits umgedreht ohne dass ein Bit mehr als einmal umgedreht wird. Die Verrauschung soll bis zu einem Anteil getestet werden, ab dem die Mustererkennung versagt.

Beim Verrauschen eines zu testenden Musters wird vorab eine bestimmte Anzahl an Indizes des Musters zufällig ausgewählt. Die Bits zu den ausgewählten Indizes werden dann umgedreht. Die Anzahl der so gedrehten Bits wird parametrisiert und niemals über- oder unterschritten.

Beim Test werden dann das Muster, optional die jeweiligen Zwischenergebnisse und das Endergebnis in Form einer gemeinsamen Diagrammdarstellung angezeigt.
