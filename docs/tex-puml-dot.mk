
DOTS			= $(wildcard diagrams/*.dot)
PUMLS			= $(wildcard diagrams/*.puml)
PUMLS			:= $(filter-out diagrams/format.puml, $(PUMLS))
PUML_TEXES		:= $(PUMLS:diagrams/%.puml=diagrams/%.latex)
DOT_PDFS		:= $(DOTS:diagrams/%.dot=diagrams/%.pdf)

# ----------------------------------------------------------------------------

# Genarate PDFs from DOTs
%.pdf: %.dot
	dot -Tpdf -o $@ $(@:.pdf=.dot)

# Genarate latex-files from PlantUML-files
%.latex: %.puml diagrams/format.puml
	@echo $@
	plantuml -tlatex:nopreamble -o "./" -x "diagrams/format.puml" "./$<"

# ----------------------------------------------------------------------------

generate-diagrams: $(DOT_PDFS) $(PUML_TEXES)

build:
	latexmk -pdf -interaction=nonstopmode \
		-pdflatex="lualatex -synctex=1 -interaction=nonstopmode -file-line-error -shell-escape %O %S" \
		$(shell pwd)/$(MAIN_LATEX_FILENAME).tex

clean:
	latexmk -c
	rm -f $(MAIN_LATEX_FILENAME).bbl

# ----------------------------------------------------------------------------

.PHONY: all clean generate-diagrams build

.DEFAULT_GOAL := all

all: clean generate-diagrams build
