% Die Muster befinden sich im Skript "hopfield_patterns.m".
% Dort sind Muster-Sätze hinterlegt,
% die hier über ihren Namen referenziert werden können.

% Name des Muster-Satzes ("pattern set name").
% psn = "3-test";
% psn = "10-test";
% psn = "10-inv";
psn = "10-gen";

% Verrauschungs-Faktor ("noise factor").
% nf = 0.05;
% nf = 0.10;
% nf = 0.15;
% nf = 0.20;
% nf = 0.25;
nf = 0.30;
% nf = 0.34;
% nf = 0.40;
% nf = 0.45;
% nf = 0.50;

rc = 4;     % Anzahl der Zeilen mit Diagrammen ("row count").
cc = 4;     % Anzahl der Spalten mit Diagrammen ("column count").
lc = 1000;  % Anzahl an Schleifendurchläufen ("loop count").

% ----------------------------------------
% Hebbsche Lernregel
% hopfield_net.hebbian_test(psn, nf, rc, cc); % Zuvor Gelerntes laden und Testen
% hopfield_net.hebbian_test_loop(psn, nf, lc); % Zuvor Gelerntes laden und mehrfach Testen

% ----------------------------------------
% Storkeys Lernregel
hopfield_net.storkey_test(psn, nf, rc, cc); % Zuvor Gelerntes laden und Testen
% hopfield_net.storkey_test_loop(psn, nf, lc); % Zuvor Gelerntes laden und mehrfach Testen
