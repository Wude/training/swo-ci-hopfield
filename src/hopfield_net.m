classdef hopfield_net < handle

  properties (GetAccess = public, SetAccess = private)
    name        = "";
    method      = "";
    node_count  = 0;
    weights     = [];
    patterns    = {};
  endproperties

  methods (Access = public)
    % --------------------------------------------------
    function self = hopfield_net(name)
      __mfile_encoding__ ("utf-8");
      pkg load statistics;
      self.name = name;
    endfunction

    % --------------------------------------------------
    function filepath = get_weight_filepath(self)
      filepath = ["./data/hopfield-", self.name, "-", self.method, ".xlsx"];
    endfunction

    % --------------------------------------------------
    function load_or_learn_hebbian(self)
      if !self.load_hebbian()
        self.learn_hebbian(hopfield_patterns(self.name));
        self.save();
      endif
    endfunction

    % --------------------------------------------------
    function load_or_learn_storkey(self)
      if !self.load_storkey()
        self.learn_storkey(hopfield_patterns(self.name));
        self.save();
      endif
    endfunction

    % --------------------------------------------------
    function success = load_storkey(self)
      success = self.load("storkey");
    endfunction

    % --------------------------------------------------
    function success = load_hebbian(self)
      success = self.load("hebbian");
    endfunction

    % --------------------------------------------------
    function success = load_storkey(self)
      success = self.load("storkey");
    endfunction

    % --------------------------------------------------
    function success = load(self, method)
      if nargin < 2 || isempty(method) method = "hebbian"; endif;
      self.method = method;

      fprintf("Load weights"); tic;

      try
        filepath = self.get_weight_filepath();

        self.node_count = xlsread(filepath, "Node-Count")(1);
        self.weights    = xlsread(filepath, "Weights");
        self.patterns   = {};
        [~, sheet_names] = xlsfinfo(filepath);
        for n = 1 : length(sheet_names)
          if strncmp(sheet_names{n}, "Pattern-", 8)
            self.patterns{end + 1} = xlsread(filepath, sheet_names{n});
          endif
        endfor
        success = true;

      catch ME
        disp(ME.message);
        success = false;
      end

      fprintf(" - "); toc;
    endfunction

    % --------------------------------------------------
    function success = save(self)
      fprintf("Save weights"); tic;

      try
        filepath = self.get_weight_filepath();

        xlswrite(filepath, [self.node_count], "Node-Count");
        xlswrite(filepath, self.weights, "Weights");
        for p = 1 : length(self.patterns)
          xlswrite(filepath, self.patterns{p}, strcat("Pattern-", num2str(p)));
        endfor
        success = true;

      catch ME
        disp(ME.message);
        success = false;
      end

      fprintf(" - "); toc;
    endfunction

    % --------------------------------------------------
    function [patterns, pattern_count] = init(self, patterns)
      pattern_count   = length(patterns);
      self.node_count = prod(size(patterns{1}(:)));
      self.patterns   = patterns;
      patterns        = {};
      for p = 1 : pattern_count
        patterns{p}   = reshape(self.patterns{p} .* 2 - 1, [self.node_count, 1]);
      endfor
      self.weights    = zeros(self.node_count, self.node_count);
    endfunction

    % --------------------------------------------------
    function learn(self, method, patterns)
      if nargin < 1 || isempty(method) method = "hebbian"; endif;

      if strcmp(method, "hebbian")
        self.learn_hebbian(patterns);
      elseif strcmp(method, "storkey")
        self.learn_storkey(patterns);
      endif
    endfunction

    % --------------------------------------------------
    function learn_hebbian(self, patterns)
      fprintf("Hebbian learning"); tic;
      self.method = "hebbian";
      [patterns, pattern_count] = self.init(patterns);

      for i = 1 : self.node_count
        for j = 1 : self.node_count
          for p = 1 : pattern_count
            if i != j
              self.weights(i, j) += patterns{p}(i) .* patterns{p}(j);
            endif
          endfor
        endfor
      endfor

      self.weights(i, j) ./= pattern_count;
      fprintf(" - "); toc;
    endfunction

    % --------------------------------------------------
    function learn_storkey(self, patterns)
      fprintf("Storkey learning"); tic;
      self.method = "storkey";
      [patterns, pattern_count] = self.init(patterns);

      for i = 1 : self.node_count
        for j = 1 : self.node_count
          for p = 1 : pattern_count
            w = patterns{p}(i) .* patterns{p}(j);
            w -= patterns{p}(i) .* self.calculate_local_field(patterns{p}(:), j, i);
            w -= patterns{p}(j) .* self.calculate_local_field(patterns{p}(:), i, j);
            w /= self.node_count;
            self.weights(i, j) += w;
          endfor
        endfor
      endfor

      fprintf(" - "); toc;
    endfunction

    % --------------------------------------------------
    function local_field = calculate_local_field(self, pattern, i, j)
      local_field = 0;

      for n = 1 : self.node_count
        if n != i && n != j
          local_field += self.weights(i, n) * pattern(n);
        endif
      endfor
    endfunction

    % --------------------------------------------------
    function [outputs, iterations] = restore(self, inputs, max_iterations, iteration_callback)
      if nargin < 3 || isempty(max_iterations)      max_iterations      = 20;             endif;
      if nargin < 4 || isempty(iteration_callback)  iteration_callback  = @(out, cnt) NA; endif;

      iteration_callback(inputs, 0);
      iterations  = 0;
      inputs      = inputs .* 2 - 1;
      changed     = true;
      outputs     = reshape(inputs, [self.node_count, 1]);

      last_outputs        = zeros(size(outputs));
      second_last_outputs = zeros(size(outputs));

      while changed && iterations < max_iterations && !isequal(outputs, second_last_outputs)
        node_indexes = randperm(self.node_count);

        second_last_outputs = last_outputs;
        last_outputs        = outputs;

        [outputs, changed] = self.calculate_outputs(node_indexes, outputs);
        iterations += 1;
        iteration_callback(reshape((outputs + 1) ./ 2, size(inputs)), iterations);
      endwhile

      outputs = reshape((outputs + 1) ./ 2, size(inputs));
    endfunction

    % --------------------------------------------------
    function [outputs, changed] = calculate_outputs(self, node_indexes, inputs)
      outputs = inputs;

      for n = 1 : length(node_indexes)
        i = node_indexes(n);
        outputs(i) = self.weights(i, :) * outputs;

        if outputs(i) > 0
          outputs(i) = 1;
        else
          outputs(i) = -1;
        endif
      endfor

      changed = !isequal(outputs, inputs);
    endfunction

    % --------------------------------------------------
    function inputs = get_noisy(self, inputs, noise_factor)
      min_inputs = min(min(inputs));

      % Die Indizes zufällig gewählter Eingabewerte in einer Anzahl, die dem Störgeräuschfaktor entspricht.
      input_indexes = randperm(numel(inputs))(1 : round(self.node_count * noise_factor));

      for i = 1 : length(input_indexes)
        p = input_indexes(i);

        if min_inputs < 0
          inputs(p) = -inputs(p);
        else
          inputs(p) = 1 - inputs(p);
        endif
      endfor
    endfunction

    % --------------------------------------------------
    % Mehrfachversuch für alle Muster mit anschließender grafischer Auswertung
    function restore_and_evaluate(self, noise_factor, loop_count)
      H = zeros(length(self.patterns), 2);

      for p = 1 : length(self.patterns)
        success_count       = 0;
        fail_count          = 0;
        false_success_count = 0;

        for n = 1 : loop_count
          noisy_inputs = self.get_noisy(self.patterns{p}, noise_factor);
          [outputs, iterations] = self.restore(noisy_inputs, 20);

          if isequal(self.patterns{p}, outputs)
            success_count += 1; % Richtig erkanntes Muster.
          else
            false_success = false;

            for m = 1 : length(self.patterns)
              if isequal(self.patterns{m}, outputs)
                false_success = true;
                break
              endif
            endfor

            if false_success
              false_success_count += 1; % Falsch erkanntes Muster.
            else
              fail_count += 1; % Kein Muster erkannt.
            endif
          endif
        endfor

        % H(p, :) = [success_count, fail_count, false_success_count];
        H(p, :) = [fail_count, false_success_count];
      endfor

      H
      % H .*= 100 ./ loop_count;

      ax = axes(gcf);
      % axis(ax, [0 length(self.patterns) 0 loop_count]);
      daspect(ax, [1, 1]);

      % b = barh(ax, [1 2 3 4 5 6 7 8 9 10], H, "Stacked");
      % set(b, {"FaceColor"}, {[0.2 0.7 0]; [0.5 0.5 0.5]; [0.7 0 0]});
      b = bar(ax, [1 2 3 4 5 6 7 8 9 10], H, "Stacked");
      set(b, {"FaceColor"}, {[0.5 0.5 0.5]; [0.7 0 0]});

      text(1 : 10, sum(H(:, 1 : 1), 2), self.get_bar_text(H(:, 1)), "VerticalAlignment", "top", "HorizontalAlignment", "center", "FontSize", 10, "Color", "w");
      text(1 : 10, sum(H(:, 1 : 2), 2), self.get_bar_text(H(:, 2)), "VerticalAlignment", "top", "HorizontalAlignment", "center", "FontSize", 10, "Color", "w");

      waitfor(gcf);
    endfunction

    % --------------------------------------------------
    function text = get_bar_text(self, data)
      text = {};

      for i = 1 : length(data)
        if data(i) != 0
          text{i} = num2str(data(i));
        else
          text{i} = "";
        endif
      endfor
    endfunction

    % --------------------------------------------------
    % Einzelversuch für alle Muster mit anschließender grafischer Auswertung
    function restore_and_show_plots(self, noise_factor, row_count, col_count)
      if nargin < 4 || isempty(row_count) row_count = 4; endif;
      if nargin < 4 || isempty(col_count) col_count = 4; endif;

      fig_count = 0;
      fig       = NA;

      for p = 1 : length(self.patterns)
        fig_count += 1;
        fig = figure(fig_count, ...
          % "Position", [110 * fig_count, 22 * fig_count, 800, 600]
          "Units", "Normalized", "Position", [0.05 * fig_count, 1 - 0.7 - 0.025 * fig_count, 0.4, 0.6]
          );
        self.restore_and_show_plot(fig, self.patterns{p}(:, :), noise_factor, row_count, col_count);
      endfor

      if !isna(fig) waitfor(fig); endif;
    endfunction

    % --------------------------------------------------
    % Einzelversuch für ein Muster mit anschließender grafischer Auswertung
    function restore_and_show_plot(self, fig, inputs, noise_factor, row_count, col_count)
      if nargin < 5 || isempty(row_count) row_count = 4; endif;
      if nargin < 6 || isempty(col_count) col_count = 4; endif;

      hopfield_net.show_subplot(fig, inputs, row_count, col_count, 1, [0.2 0.4 0.7]);
      noisy_inputs = self.get_noisy(inputs, noise_factor);
      [outputs, iterations] = self.restore(noisy_inputs, row_count * col_count - 2, ...
        @(out, cnt) self.show_subplot(fig, out, row_count, col_count, cnt + 2, self.color_function(inputs, out)));
    endfunction

    % --------------------------------------------------
    function color = color_function(self, inputs, outputs)
      if isequal(inputs, outputs)
        color = [0.2 0.7 0]; % Richtig erkanntes Muster.
        return
      endif

      for p = 1 : length(self.patterns)
        if isequal(self.patterns{p}, outputs)
          color = [0.7 0 0]; % Falsch erkanntes Muster.
          return
        endif
      endfor

      color = [0.5 0.5 0.5]; % Kein Muster erkannt.
    endfunction
  endmethods

  methods (Static, Access = public)
    % --------------------------------------------------
    function ax = show_subplot(fig, inputs, row_count, col_count, subplot_index, color)
      inputs = flip(inputs.', 2);

      ax = subplot(row_count, col_count, subplot_index, axes(fig));
      axis(ax, [0 size(inputs, 1) 0 size(inputs, 2)]);
      daspect(ax, [1, 1]);
      hold(ax, "on");
      grid(ax, "off");

      for i = 1 : size(inputs, 1)
        for j = 1 : size(inputs, 2)
          if inputs(i, j) == 1
            fill(ax, [i - 1, i, i, i - 1, i - 1], [j - 1, j - 1, j, j, j - 1], color); hold(ax, "on");
            % rectangle(ax, "Position", [i - 1, j - 1, 1, 1], "FaceColor", color, "EdgeColor", [0, 0, 0]);
          endif
        endfor
      endfor

      refresh(fig);
    endfunction

    % --------------------------------------------------
    function show_patterns(patterns, fig_count, row_count, col_count)
      if nargin < 2 || isempty(fig_count) fig_count = 1; endif;
      if nargin < 3 || isempty(row_count) row_count = 2; endif;
      if nargin < 4 || isempty(col_count) col_count = 5; endif;

      fig = figure(fig_count, ...
        % "Position", [110 * fig_count, 22 * fig_count, 800, 600]
        "Units", "Normalized", "Position", [0.05 * fig_count, 1 - 0.7 - 0.025 * fig_count, 0.4, 0.4]
        );

      for p = 1 : length(patterns)
        hopfield_net.show_subplot(fig, patterns{p}, row_count, col_count, p, [0.2 0.4 0.7]);
      endfor
    endfunction

    % --------------------------------------------------
    function M = show_orthogonality(patterns, fig_count)
      if nargin < 2 || isempty(fig_count) fig_count = 1; endif;

      input_size = size(patterns{1}, 1) * size(patterns{1}, 2);
      P = {};
      for p = 1 : length(patterns)
        P{p} = reshape(patterns{p}, [input_size, 1]) * 2 - 1;
      endfor
      M = zeros(length(patterns), length(patterns));

      for i = 1 : length(patterns)
         for j = 1 : length(patterns)
            M(i, j) = P{i}(:).' * P{j}(:);
         endfor
      endfor

      fig = figure(fig_count, ...
        % "Position", [110 * fig_count, 22 * fig_count, 800, 600]
        "Units", "Normalized", "Position", [0.05 * fig_count, 1 - 0.7 - 0.025 * fig_count, 0.6, 0.4]
        );

      disp(M);
      % pcolor(M);
      % surf(M);

      tab = uitable(fig, "Data", M, ...
        "ColumnName", {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, ...
        "RowName", {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, ...
        "Units", "Normalized", "Position", [0, 0, 1, 1]);

      % [row, col] = find(M > 0.3 * input_size);

      % s = uistyle("BackgroundColor", [1 0.6 0.6]);
      % addStyle(uit, s, "cell", [row, col]);
    endfunction

    % --------------------------------------------------
    function hebbian_test(pattern_set_name, noise_factor, row_count, col_count)
      if nargin < 1 || isempty(pattern_set_name)  pattern_set_name  = "10"; endif;
      if nargin < 2 || isempty(noise_factor)      noise_factor      = 0.34; endif;
      if nargin < 3 || isempty(row_count)         row_count         = 4;    endif;
      if nargin < 4 || isempty(col_count)         col_count         = 4;    endif;

      n = hopfield_net(pattern_set_name);
      n.load_or_learn_hebbian()
      n.restore_and_show_plots(noise_factor, row_count, col_count);
    endfunction

    % --------------------------------------------------
    function storkey_test(pattern_set_name, noise_factor, row_count, col_count)
      if nargin < 1 || isempty(pattern_set_name)  pattern_set_name  = "10"; endif;
      if nargin < 2 || isempty(noise_factor)      noise_factor      = 0.34; endif;
      if nargin < 3 || isempty(row_count)         row_count         = 4;    endif;
      if nargin < 4 || isempty(col_count)         col_count         = 4;    endif;

      n = hopfield_net(pattern_set_name);
      n.load_or_learn_storkey()
      n.restore_and_show_plots(noise_factor, row_count, col_count);
    endfunction

    % --------------------------------------------------
    function hebbian_test_loop(pattern_set_name, noise_factor, loop_count)
      if nargin < 1 || isempty(pattern_set_name)  pattern_set_name  = "10"; endif;
      if nargin < 2 || isempty(noise_factor)      noise_factor      = 0.34; endif;
      if nargin < 3 || isempty(loop_count)        loop_count        = 1000; endif;

      n = hopfield_net(pattern_set_name);
      n.load_or_learn_hebbian()
      n.restore_and_evaluate(noise_factor, loop_count);
    endfunction

    % --------------------------------------------------
    function storkey_test_loop(pattern_set_name, noise_factor, loop_count)
      if nargin < 1 || isempty(pattern_set_name)  pattern_set_name  = "10"; endif;
      if nargin < 2 || isempty(noise_factor)      noise_factor      = 0.34; endif;
      if nargin < 3 || isempty(loop_count)        loop_count        = 1000; endif;

      n = hopfield_net(pattern_set_name);
      n.load_or_learn_storkey()
      n.restore_and_evaluate(noise_factor, loop_count);
    endfunction
  endmethods

endclassdef
